import './App.css';
import { useQuery, gql } from '@apollo/client';
import react, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import Detail from './Detail';

const App = () => {

  const [showModal, setShowModal] = useState(false);
  const [dataProduct, setDataProduct] = useState({});

  const showDetail = (product) => {
    setDataProduct(product);
    setShowModal(true);
  }

  const GET_PRODUCTS = gql`
    query{
      products{
        data {
          id,
          product_name,
          seller_name,
          image,
          price,
          type_product,
          stock,
          type_rent,
          date_availability,
          location,
          created_at,
          updated_at
        }
      }
    }
  `;

  const Products = () => {
    const { loading, error, data } = useQuery(GET_PRODUCTS);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error : {error.message}</p>;

    return data.products.data.map((product) => (
      <Card key={product.id} style={{ width: '18rem' }} onClick={() => showDetail(product)}>
        <Card.Img variant="top" src={product.image} />
        <Card.Body>
          <Card.Title>{product.product_name}</Card.Title>
          <Card.Text>
            Precio: {product.price}
          </Card.Text>
        </Card.Body>
      </Card>
    ));
  }

  return (
    <>
      <h3 style={{margin:"5%"}}>Mira más detalles haciendo click en los productos. </h3>
      <div style={{ maxWidth:"1080px",  margin:"5%", display: 'inline-flex'}}>
        <Products/>
      </div>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{dataProduct.product_name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Detail initialState={dataProduct} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Cerrar
          </Button>
          <Button variant="primary">
            Reservar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default App;
