const Detail = ({initialState}) => {
    return (
        <>  
            <p>id: {initialState.id}</p>
            <p>Precio: {initialState.price}</p>
            <p>Nombre del producto: {initialState.product_name}</p>
            <p>Nombre del vendedor: {initialState.seller_name}</p>
            <p>Stock: {initialState.stock}</p>
            <p>Disponibilidad: {initialState.date_availability}</p>
        </>
    );
}

export default Detail;

