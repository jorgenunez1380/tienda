<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'produt_name',
        'seller_name',
        'image',
        'price',
        'type_product',
        'stock',
        'type_rent',
        'date_availability',
        'location'
    ];
}
