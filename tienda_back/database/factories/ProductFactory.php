<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'product_name' => fake()->text(15),
            'seller_name' => fake()->name(),
            'image' => fake()->imageUrl($width = 200, $height = 200),
            'price' => fake()->numberBetween($min = 100, $max = 6000),
            'type_product' => fake()->numberBetween($min = 1, $max = 3),
            'stock' => fake()->numberBetween($min = 1, $max = 86),
            'type_rent' => fake()->numberBetween($min = 1, $max = 2),
            'date_availability' => now(),
            'location' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.956887141502!2d-74.074587525248!3d4.601744342507962!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f99a6c60f7ccd%3A0xc18fcd6091a051ed!2sMuseo%20del%20Oro!5e0!3m2!1ses!2sco!4v1682224034635!5m2!1ses!2sco"
        ];
    }
}
